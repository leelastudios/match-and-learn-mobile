module(..., package.seeall)

local filePath = system.pathForFile( "data04.txt", system.DocumentsDirectory )
local file = io.open( filePath, "r" )
local dataArr = {}
local startValues = --[[ 1-10, base values]]--
					{0,0,0,0,0,0,0,0,0,0,
					--[[ 11-20, level scores]]--
					0,0,0,0,0,0,0,0,0,0}
					
function loadSavedData()
	
	if file then
		local contents = file:read( "*a" )

		print("Loading file...")

		local newString = contents
		
		for i=1, #startValues do
			
			local myVal = newString:sub(1, newString:find(",") - 1)
			local numVal = 0
			numVal = numVal + myVal

			dataArr[i] = myVal
			--print(myVal)
			newString = newString:sub(newString:find(",") + 1)
		end

		io.close( file )
	else
		print( "Creating file..." )

		-- create file b/c it doesn't exist yet
		file = io.open( filePath, "w" )
		-- my fake data... total values long
		local numbers = startValues
		for i = 1, #startValues do
			--print(numbers[i])
			--numbers[i] = "0"
			file:write( numbers[i], "," )
		end
		dataArr = numbers
		io.close( file )
		
	end
	
	onDataLoaded(dataArr)
	
end

function writeNewData(sentData)
	
	print( "Creating new save file..." )

	file = io.open( filePath, "w" )

	local numbers = sentData
	for i = 1, #startValues do
		file:write( numbers[i], "," )
	end
	
	dataArr = numbers
	io.close( file )
	
end

function resetStorage()
	
	print(" RESET SAVE FILE !!! ")
	
	file = io.open( filePath, "w" )
	
	local numbers = startValues
	for i = 1, #startValues do
		--print(numbers[i])
		--numbers[i] = "0"
		file:write( numbers[i], "," )
	end
	
	dataArr = startValues
	io.close( file )
	
	onDataLoaded(dataArr)
	
end

-- READ THE FILE FUNCTION --
function readFile( strFilename )
	-- will load specified file, or create new file if it doesn't exist
	
	local theFile = strFilename;
	
	local path = system.pathForFile( theFile, system.DocumentsDirectory )
	
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" );
	
	if (file) then
---{
	   -- read all contents of file into a string
	   local contents = file:read( "*a" )
	   io.close( file )
	   return contents;
---}
	else
---{
	   -- create file b/c it doesn't exist yet
	   file = io.open( path, "w" );
	   file:write( "0" );
	   io.close( file );
	   
	   return "0";
---}
	end
end


-- WRITE TO FILE FUNCTION --
function writeFile( strFilename, strValue )
	if strValue == nil then
		print("Cannot write to file: missing value")
		return false;
	end
	-- will save specified value to specified file
	local theFile = strFilename
	local theValue = strValue
	
	local path = system.pathForFile( theFile, system.DocumentsDirectory );
	
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "w+" )
	if file then
	   -- write game score to the text file
	   file:write( theValue )
	   io.close( file )
	end
end
