-- Importing libraries
--local http = require("socket.http") -- For internet connection detection

-- Main array that holds the functions of this library
local inviteFriendsLibrary = {}

-- The message to be sent via email or sms
local sharedMessage = "Hey I'm limon. Check this game out cause I say so!"
local emailTitle = "Check This Out"

-- SMS Friend function
local function smsFriend()
	print("SMS popup")
	local options =
	{
	   body = sharedMessage
	}
	native.showPopup("sms", options)
end

-- Email Friend function
local function emailFriend()
	print("Email popup")
	
	local options =
	{
	   --to = "john.doe@somewhere.com",
	   subject = emailTitle,
	   body = sharedMessage,
	   --attachment = { baseDir=system.DocumentsDirectory, filename="Screenshot.png", type="image" },
	}
	native.showPopup("mail", options)
end

-- Back up function to test internet connection
local function testNetworkConnection()
    local connection = require('socket').connect('google.com', 80)
    if connection == nil then
        return false
    end
    connection:close()
    return true
end

-- Check for reception/internet connection
local function givePriorityNetwork() 
	local g_Counter = 0;
	
	local function MyNetworkReachabilityListener(event)
		if(event.isReachableViaCellular) then
			-- If cellular is available then
			-- message the person
 			smsFriend()
 			-- Trigger the removal of the listener
 			g_Counter = 5;	
 		elseif(event.isReachableViaWiFi or event.isConnectionOnDemand) then
 			-- If there is Wi-Fi or Connection on demand
 			-- then email the person
 			emailFriend()
 			-- Trigger the removal of the listener
 			g_Counter = 5;
 		end
 		
        g_Counter = g_Counter + 1
        if g_Counter > 3 then
                print("removing event listener")
                network.setStatusListener( "www.apple.com", nil)
        end
              
	end
	 
	if network.canDetectNetworkStatusChanges then
		-- If you can detect network status
		-- then start the listener.
		-- The listener will handle the call to email or sms
		network.setStatusListener( "www.apple.com", MyNetworkReachabilityListener )
	else
		
		print("network reachability not supported on this platform")
		-- Return a trigger to let the caller know that it 
		-- has to handle the call on it's own.
		return "not available"
	end

end
-- Invite friends function that 
-- decides whether to send an email or a sms.
local function inviteFriends()
	-- Use priority check to decide what to do.
	-- This doesn't work for Android.
	local status = givePriorityNetwork()
	-- If givePriorityNetwork did not handle the call
	-- to sms or email then let's handle it other way
	if(status == "not available") then
		-- Check if internet is not available
		if not testNetworkConnection() then
			-- Then use sms
			smsFriend()
		else
			-- If internet is available, use email
			emailFriend()
		end		
	end	
end


-- Assign those functions to the inviteFriendsLibrary 
inviteFriendsLibrary.inviteFriends = inviteFriends

-- returns localized object for use
return inviteFriendsLibrary
--------------------------------------------

