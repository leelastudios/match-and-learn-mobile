
display.setStatusBar( display.HiddenStatusBar )
--system.activate ("multitouch")

-------------------------- IMPORTS & SETTINGS

local data = require("data")
--local spriteClass = require("sprite")
local fps = require("fps")
local storage = require("storage")
local shell = require("shell")
local engine = require("engine")
local inviteFriends = require("invitefriends")
local gameStore = require("gamestore")


-------------------------- GLOBAL VARIABLES

-- Local file names
local soundFile = "sound-status.data"
local premiumFile = "game-status.data"
-- File that holds which level to load
-- 1 is the first level
local levelFile = "current-level.data"
local gameModeFile = "game-mode.data"

-- Functions to save/load level data
function getCurrentLevel()
	local val = storage.readFile(levelFile)
	return val
end

function saveLevelProgress(number)
	storage.writeFile(levelFile, number)
end

function getGameMode() 
	local value = storage.readFile(gameModeFile)
	if(value == "0") then
		value = "learn"
		storage.writeFile(gameModeFile, value)
	end
	
	return value
end

function saveGameMode(mode)
	storage.writeFile(gameModeFile, mode)
end
-- 1 means premium, 0 means non-premium version
-- CHange isPremium = true;  
isPremium = (storage.readFile(premiumFile) == "1")
isPremium = true

inviteFriendsAllowed = false;

allSaveData = {}
isPaused = true
gameMode = getGameMode()
recordTime = 0
firstPlay = true
firstHelp = false
--  0 means sound on, 1 means sound off
audioOn = (storage.readFile(soundFile) == "0")

local audioG
local bgChannel = 1
local endChannel = 3
local audioTimer

local bgVolume = 1
local sfxVolume = 1

-- If the audio is not not on,
-- set volume to 0
if not audioOn then
	bgVolume = 0
	sfxVolume = 0
	audio.setVolume(bgVolume, {channel=bgChannel})
	audio.setVolume(bgVolume, {channel=endChannel})
end


function premiumUnlocked() 
	storage.writeFile(premiumFile, "1")
	isPremium = true;
	-- Add logic to send information to server so in case person removes 
	-- saved data, we can save their data status.	
		
	native.showAlert("Notice","You have unlocked the premium version of the game. Enjoy all the new features! Thanks for supporting our hard work.", {"OK"})	
end

function removePremiumVersion()
	storage.writeFile(premiumFile, "0")
	isPremium = false;
	-- Add logic to remove the premium version from the server
	-- saved data, we can save their data status.
	
	-- Show popup		
	native.showAlert("Notice","Your in-app purchase has been refunded and premium version has been removed!", {"OK"})	
	
end

-- Premium
function unlockPremiumClosure() 
	gameStore.unlockPremium()
end

-------------------------- EVENTS

function buildFPS()

	--local performance = fps.PerformanceOutput.new();
	--performance.group.x, performance.group.y = 50,  0;
	
end

-------------------------- DATA

function onDataLoaded(dataArr)
	
	_G.allSaveData = dataArr
	
	recordTime = tonumber(dataArr[1])
	print("OLD TIME: " .. recordTime)
	
	startEngine()
	
end

-------------------------- AUDIO

function buildAudio()

	audioG = display.newGroup()
	
	audio.reserveChannels( 3 )
	
	local sound = audio.loadSound("audio/goodcardclick.mp3")
	audioG.goodCard = sound
	
	local sound = audio.loadSound("audio/wrongcard.mp3")
	audioG.wrongCard = sound
	
	local sound = audio.loadSound("audio/buttonclick.mp3")
	audioG.click = sound 
	
	local sound = audio.loadStream("audio/intro.mp3")
	audioG.intro = sound

	local sound = audio.loadStream("audio/play.mp3")
	audioG.play = sound

	local sound = audio.loadStream("audio/endscreen.mp3")
	audioG.endscreen = sound
	
end

function playAudio(type)

	local availableChannel = audio.findFreeChannel()
	audio.setVolume(sfxVolume, {channel=availableChannel })

	if(type == "goodCard")then
		audio.play( audioG.goodCard, { channel=availableChannel, loops=0}  )
	end
	
	if(type == "wrongCard")then
		audio.play( audioG.wrongCard, { channel=availableChannel, loops=0}  )
	end
	
	if(type == "click")then
		audio.play( audioG.click, { channel=availableChannel, loops=0}  )
	end
	
	--- ending
	
	if(type == "endscreen")then
		--audio.setVolume(1, {channel=endChannel })
		audio.play( audioG.endscreen, { channel=endChannel, loops=0}  )
	end
	
	--- loops
	
	if(type == "intro")then
		
		audio.play( audioG.intro, { channel=bgChannel, loops=-1}  ) 
		audio.setVolume(bgVolume, {channel=bgChannel})
		
		
	end
	
	if(type == "play")then
		
		audio.play( audioG.play, { channel=bgChannel, loops=-1}  )
		audio.setVolume(bgVolume, {channel=bgChannel})
		
	end
			
end

function startIntroLoop()

	--local myclosure = function() return playAudio("intro") end
	--audioTimer = timer.performWithDelay( 20220, myclosure, 0 )
	playAudio("intro")
	
end

function startPlayLoop()

	--local myclosure = function() return playAudio("play") end
	--`audioTimer = timer.performWithDelay( 63350, myclosure, 0 )
	playAudio("play")
	
end

function fadeOutMusic()

	--timer.cancel(audioTimer)
	audio.fadeOut( bgChannel, 1500 )
	
end

function fadeEndOut()
	
	audio.fadeOut( endChannel, 1500 )

end

function toggleAudioOnOff()

	if(audioOn)then
		bgVolume = 0
		sfxVolume = 0
		audioOn = false
		storage.writeFile(soundFile, "1")
	else
		bgVolume = 1
		sfxVolume = 1
		audioOn = true
		storage.writeFile(soundFile, "0")
	end
	
	-- Set the channel volume
	audio.setVolume(bgVolume, {channel=bgChannel})
	audio.setVolume(bgVolume, {channel=endChannel})	
end

-------------------------- INIT

function startEngine()

	buildAudio()

	shell.buildShell()

end

storage.loadSavedData()

-------------------------- DEBUG
--==================--
-- MEMORY MONITOR --
--==================--
local monitorMem = function()

    collectgarbage()
    print( "MemUsage: " .. collectgarbage("count") )

    local textMem = system.getInfo( "textureMemoryUsed" ) / 1000000
    print( "TexMem:   " .. textMem )
end

--Runtime:addEventListener( "enterFrame", monitorMem )


