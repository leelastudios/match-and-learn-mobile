-- The library object that 
-- holds all the necessary functions
local gameStore = {}

-- Import the store
local store = require("store")

-------------------------------------------------------------------------------
--  Product IDs should match the In App Purchase products set up in iTunes Connect.
--  We cannot get them from the iTunes store so here they are hard coded;
--  your app could obtain them dynamically from your server.
-------------------------------------------------------------------------------

-- To be assigned a product list from one of the arrays below after we've connected to a store.
-- Will be nil if no store is supported on this system/device.
local currentProductList = nil

-- List of valid products(should contain one item)
local validProducts = {}

-- Product IDs for the "apple" app store.
local appleProductList =
{
	-- These Product IDs must already be set up in your store
	-- We'll use this list to retrieve prices etc. for each item
	-- Note, this simple test only has room for about 4 items, please adjust accordingly
	-- The iTunes store will not validate bad Product IDs 
	"com.anscamobile.NewExampleInAppPurchase.ConsumableTier1",
	"com.anscamobile.NewExampleInAppPurchase.NonConsumableTier1",
	"com.anscamobile.NewExampleInAppPurchase.SubscriptionTier1",
}

-- Product IDs for the "google" Android Marketplace store.
local googleProductList =
{
	-- These product IDs are used for testing and is supported by all Android apps.
	-- Purchasing these products will not bill your account.
	"android.test.purchased",			-- Marketplace will always successfully purchase this product ID.
	"android.test.canceled",			-- Marketplace will always cancel a purchase of this product ID.
	"android.test.item_unavailable",	-- Marketplace will always indicate this product ID as unavailable.
}


-------------------------------------------------------------------------------
-- Displays a warning indicating that store access is not available,
-- meaning that Corona does not support in-app purchases on this system/device.
-- To be called when the store.isActive property returns false.
-------------------------------------------------------------------------------
local function showStoreNotAvailableWarning()
	native.showAlert("Notice", "In-app purchases is not supported on this device.", { "OK" } )
end

-------------------------------------------------------------------------------
-- Handler for all store transactions
-- This callback is set up by store.init()
-------------------------------------------------------------------------------
local function transactionCallback(event)

	-- Log transaction info.
	print("transactionCallback: Received event " .. tostring(event.name))
	print("state: " .. tostring(event.transaction.state))
	print("errorType: " .. tostring(event.transaction.errorType))
	print("errorString: " .. tostring(event.transaction.errorString))

	if event.transaction.state == "purchased" then
		premiumUnlocked()

		local infoString = "Transaction successful!"
		print(infoString)
		--descriptionArea.text = infoString
	elseif  event.transaction.state == "restored" then
		-- Reminder: your app must store this information somewhere
		-- Here we just display some of it
		local infoString = "Restoring transaction:" ..
							"\n   Original ID: " .. tostring(event.transaction.originalTransactionIdentifier) ..
							"\n   Original date: " .. tostring(event.transaction.originalDate)
		print(infoString)
		--descriptionArea.text = infoString
		print("productIdentifier: " .. tostring(event.transaction.productIdentifier))
		print("receipt: " .. tostring(event.transaction.receipt))
		print("transactionIdentifier: " .. tostring(event.transaction.transactionIdentifier))
		print("date: " .. tostring(event.transaction.date))
		print("originalReceipt: " .. tostring(event.transaction.originalReceipt))

	elseif  event.transaction.state == "refunded" then
		-- Refunds notifications is only supported by the Google Android Marketplace.
		-- Apple's app store does not support this.
		-- This is your opportunity to remove the refunded feature/product if you want.
		removePremiumVersion()

	elseif event.transaction.state == "cancelled" then
		-- Show a popup for cancelled transaction
		native.showAlert("Notice","Transaction was cancelled.", {"OK"})	


	elseif event.transaction.state == "failed" then    
		-- Show some popup about failure.    
		native.showAlert("Failed","Transaction failed!", {"OK"});
	else
		local infoString = "Unknown event"
		print(infoString)
	end

	-- Tell the store we are done with the transaction.
	-- If you are providing downloadable content, do not call this until
	-- the download has completed.
	store.finishTransaction( event.transaction )
end
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Handler to receive product information 
-- This callback is set up by store.loadProducts()
-------------------------------------------------------------------------------
local function loadProductsCallback( event )
	-- Debug info for testing
	print("In loadProductsCallback()")
	print("event, event.name", event, event.name)
	print(event.products)
	print("#event.products", #event.products)

	-- save for later use
	validProducts = event.products
	
	-- Only for 
	--invalidProducts = event.invalidProducts    
	--addProductFields()
end


-- Function to set up the store
local function setupMyStore()
	if store.isActive then
		if store.canLoadProducts then
			-- Property "canLoadProducts" indicates that localized product information such as name and price
			-- can be retrieved from the store (such as iTunes). Fetch all product info here asynchronously.
			store.loadProducts( currentProductList, loadProductsCallback )
			print ("After store.loadProducts, waiting for callback")
		else 
			-- Unable to retrieve products from the store because:
			-- 1) The store does not support apps fetching products, such as Google's Android Marketplace.
			-- 2) No store was loaded, in which case we could load dummy items or display no items to purchase
			validProducts[1] = {}
			validProducts[1].title = "Premium"
			validProducts[1].description = "Unlock the premium version of the game"
			if currentProductList then
				validProducts[1].productIdentifier = currentProductList[1]
			end
			validProducts[2] = {}
			validProducts[2].title = "Item 2"
			validProducts[2].description = "Blahh blah blah"
			if currentProductList then
				validProducts[2].productIdentifier = currentProductList[2]
			end
			validProducts[3] = {}
			validProducts[3].title = "Daily dose"
			validProducts[3].description = "Of some fun!"
			if currentProductList then
				validProducts[3].productIdentifier = currentProductList[3]
			end
		end
	end
end

-------------------------------------------------------------------------------
-- When unlock premium button is pressed
-- we call this function that allows you
-- to buy the premium version of the game
-------------------------------------------------------------------------------
local function unlockPremium() 
	-- Check if already Premium is already unlocked
	-- ADD LOGIC
	
	
	-- Not products available, so exit this function call.
	if(not currentProductList) then
		return false;
	end
	
	-- Load productId for Premium Game 
	-- use currentProductList[1] for the actual product
	-- other numbers are for testing
	local productId = currentProductList[1];
	
	-- Check if store is available
	if store.isActive == false then
		-- Throw warning if store is not active
		showStoreNotAvailableWarning()
	elseif store.canMakePurchases == false then
		-- Alert if you cannot make purchases at this time
		native.showAlert("Notice","Store purchases are not available, please try again later", {"OK"})
	elseif productId then
		-- Purchase the premium product
		print("Ka-ching! Purchasing " .. tostring(productId))
		store.purchase({productId})
	end
end


-- Connect to store at startup, if available.
if store.availableStores.apple then
	currentProductList = appleProductList
	store.init("apple", transactionCallback)
	print("Using Apple's in-app purchase system.")
	
elseif store.availableStores.google then
	currentProductList = googleProductList
	store.init("google", transactionCallback)
	print("Using Google's Android In-App Billing system.")
	
else
	print("In-app purchases is not supported on this system/device.")
end

-- Add the necessary function to the library
gameStore.unlockPremium = unlockPremium

-- return the library object
return gameStore