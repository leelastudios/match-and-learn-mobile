
module(..., package.seeall)

-------------------------- IMPORTS & SETTINGS

-------------------------- GLOBAL VARIABLES

local menuG
local gameG
local overG
local loadG

local artTimer
local boyAnims = {}
local boyTimers = {}
local introTrans = {}
local sandTrans

local loaderTrans

-- Local functions
local buildLoader, buildMenu, buildGame, buildBoy, buildOverlay
local startMenu, resumeMenu, loopIntro, hideMenu, showOverlay, closeOverlay
local unpauseShell, showGame, startGame
local animateBoy
local onShellMenuTouch, onOverlayTouch

-------------------------- CONSTRUCTION
-- Public function called from main
function buildShell()

	menuG = display.newGroup() -- lies most behind
	gameG = display.newGroup()
	overG = display.newGroup()
	loadG = display.newGroup() -- foreground
	
	buildLoader()
	
end

buildLoader = function()
	
	local bg = display.newImageRect("graphics/loader/bg.jpg", 512, 384)
	bg.x = 256
	bg.y = 192
	bg.alpha = 0
	menuG:insert(bg)
	
	local bg2 = display.newImageRect("graphics/shell/game_bg.jpg", 512, 384)
	bg2.x = 256
	bg2.y = 192
	bg2.alpha = 0
	menuG:insert(bg2)
	menuG.bg2 = bg2
	
	local loading = display.newImageRect("graphics/loader/loading.png", 112, 30)
	loading.x = 256
	loading.y = 263 + 25
	loading.alpha = 0
	loadG:insert(loading)
	loadG.text = loading
	
	local boyG = display.newGroup()
	boyG.x = 256
	boyG.y = 170 + 25
	boyG.alpha = 0
	loadG:insert(boyG)
	loadG.boyG = boyG
	
	-- Didn't change this coordinates
	local boy = display.newImageRect("graphics/loader/boy.png", 176, 143)
	boy.x = 0
	boy.y = 0
	boyG:insert(boy)
	
	-- Didn't change this coordinates
	local barBg = display.newImageRect("graphics/loader/bg.png", 134, 21)
	barBg.x = 1
	barBg.y = 14.5

	--barBg.x = 0
	--barBg.y = 0
	boyG:insert(barBg)
	
	local bar = display.newImageRect("graphics/loader/bar.png", 133, 21)
	bar.anchorX = 0;
	bar.x = 190.5 - 256
	bar.y = 14.5
	bar.xScale = .01
	boyG:insert(bar)
	loadG.bar = bar
	
	local outline = display.newImageRect("graphics/loader/outline.png", 135, 23)
	outline.x = 1
	outline.y = 14.5
	boyG:insert(outline)
	
	transition.to(bg,       {delay=0250, time=0500, y=192, alpha=1, transition=easing.outQuad})
	transition.to(boyG,     {delay=0750, time=0500, y=170.5, alpha=1, transition=easing.outQuad})
	transition.to(loading,  {delay=1250, time=0500, y=263, alpha=1, transition=easing.outQuad, onComplete=buildMenu})
	
	loaderTrans = transition.to(loadG.bar, {delay=1000, time=3000, xScale=.25})

end

buildMenu = function()
	
	--build art for animation
	
	local artG = display.newGroup()
	artG.y = -12
	menuG:insert(artG)
	menuG.artG = artG
	
	local red = display.newRect(5, 51.5, 499, 280.5)
	red:setFillColor(224,45,45)
	red.alpha = 0;
	artG:insert(red)

	local artX = {402.5, 115.5, 386, 247.5, 106, 258}
	local artY = {202, 213.5, 241.5, 205, 246, 282}
	local artWidth = {206, 192, 180, 431, 163, 170}
	local artHeight = {258, 231, 189, 209, 134, 84} 
	
	
	for i=1, #artX do
		
		local art = display.newImageRect("graphics/art/art" .. i .. ".png", artWidth[i], artHeight[i])
		art.x = artX[i]
		art.y =	artY[i]
		artG:insert(art)
		
	end
	
	local yellow = display.newRect(5, 51.5, 449, 280.5)
	yellow:setFillColor(245,214,169)
	yellow.alpha = 0
	artG:insert(yellow)
	
	local frame = display.newImageRect("graphics/art/frame.png", 516, 300)
	frame.x = 254
	frame.y = 195
	artG:insert(frame)
	
	--help
	
	local btnG = display.newGroup()
	btnG.x = 98
	btnG.y = 360
	btnG.id = "help"
	menuG:insert(btnG)
	menuG.help = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "HELP", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -5
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onShellMenuTouch)
	
	--credits
	
	local btnG = display.newGroup()
	btnG.x = 256
	btnG.y = 360
	btnG.id = "credits"
	menuG:insert(btnG)
	menuG.credits = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "CREDITS", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -5
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onShellMenuTouch)
	
	--settings
	local btnG = display.newGroup()
	btnG.x = 413
	btnG.y = 360
	btnG.id = "settings"
	menuG:insert(btnG)
	menuG.settings = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "SETTINGS", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onShellMenuTouch)
	
	--boy animation
	
	local boyG = display.newGroup()
	menuG:insert(boyG)
	menuG.boy = boyG
	
	
	-- Not using dynamic scaling on this
	local body = display.newImage("graphics/boy/play_body.png", true)
	body.x = 0
	body.y = 0
	boyG:insert(body)
	
	
	-- Didn't change this coordinates or not scaled
	-- local mySpriteSheet = display.newSpriteSheet( "graphics/boy/play.png", 160, 160 )
	local sheetData = { width=160, height=160, numFrames=17}
	local mySheet = graphics.newImageSheet( "graphics/boy/play.png", sheetData )
	
	local sequenceData   =  {

			{
				name = "go",
			 	start = 1, --starting frame index
				count = 17, --total number of frames to animate consecutively before stopping or looping
				time = 1000
				--loopCount = 0
		}
	}
		
		
	local mySprite = display.newSprite( mySheet, sequenceData );
	mySprite.x = 0
	mySprite.y = -100
	boyG:insert(mySprite)
	boyG.head = mySprite
	
	
	-- Old scale was .9
	boyG.xScale = .45
	boyG.yScale = .45
	boyG.x = 256
	boyG.y = 268
	
	--play
	
	local btnG = display.newGroup()
	btnG.x = 256
	btnG.y = 270
	btnG.xScale = 1.1
	btnG.yScale = 1.1
	btnG.id = "play"
	menuG:insert(btnG)
	menuG.play = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_sm.png", 121, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "PLAY", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -5
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onShellMenuTouch)
	
	-- Didn't change this coordinates
	local hand = display.newImageRect("graphics/boy/boy_hand.png", 137, 22)
	
	hand.x = 256
	hand.y = 266
	menuG:insert(hand)
	menuG.hand = hand
	
	---- TITLE
	
	local txtField = display.newText( "MATCH", 0, 0, "Turtles", 80 )
	txtField.x = 100 + 35 + 2.5
	txtField.y = 57 - 100
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	
	menuG:insert(txtField)
	menuG.title1 = txtField
	
	
	
	local txtField = display.newText( "AND", 0, 0, "Turtles", 80 )
	txtField.x = 200 + 35 + 24
	txtField.y = 57 - 100
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	menuG:insert(txtField)
	menuG.title2 = txtField
	
	local txtField = display.newText( "LEARN!", 0, 0, "Turtles", 80 )
	txtField.x = 332 + 35 + 10
	txtField.y = 57 - 100
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	menuG:insert(txtField)
	menuG.title3 = txtField
	
	-- init
	
	menuG.artG.y = -400
	menuG.play.y = 425
	menuG.boy.alpha = 0
	menuG.hand.alpha = 0
	
	menuG.help.y = 425
	menuG.credits.y = 425
	menuG.settings.y = 425
	
	-- move loader
	transition.cancel(loaderTrans)
	loaderTrans = transition.to(loadG.bar, {time=250, xScale=.40, onComplete=buildGame})
		
end

buildGame = function()

	local bg1 = display.newImageRect("graphics/game/bg1.png", 531, 300)
	bg1.x = 257
	bg1.y = -400 --388 
	gameG:insert(bg1)
	gameG.bg1 = bg1
	
	--build top (timer display)
	
	local timerG = display.newGroup()
	timerG.x = 256
	timerG.y = -100 --52
	gameG:insert(timerG)
	gameG.timer = timerG
	
	local bg2 = display.newImageRect("graphics/game/bg2.png", 507, 51)
	bg2.x = 0
	bg2.y = 0
	timerG:insert(bg2)
	
	local numberG = display.newGroup()
	numberG.y = -1
	timerG:insert(numberG)
	timerG.numbers = numberG
	
	local txtField = display.newText( engine.formatTime("BEST TIME: ", _G.recordTime), 0, 0, "Turtles", 42 )
	txtField.anchorX =0.5;
	txtField.anchorY = 0.5;
	txtField.x = -142	
	txtField.y = 0
	txtField:setFillColor(0,204,51)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	numberG:insert(txtField)
	numberG.bestTime = txtField
	
	local txtField = display.newText( "MY TIME: 00:00", 0, 0, "Turtles", 42 )
	txtField.anchorX =0.5;
	txtField.anchorY = 0.5;
	txtField.x = 160	
	txtField.y = 0
	txtField:setFillColor(1,0.8,0.2)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	numberG:insert(txtField)
	numberG.newTime = txtField
	
	--- hourglass
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/engine/hourglass.png", 80, 100 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 54)
	--sprite.add( mySet, "go", 1, 54, 1000, 1 )
	
	local sheetData = { width = 80, height = 100, numFrames=54}
	local mySheet = graphics.newImageSheet( "graphics/engine/hourglass.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 54, --total number of frames to animate consecutively before stopping or looping
			time = 1000,
			loopCount = 1
	}
}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	--local mySprite = sprite.newSprite( mySet )
	mySprite.x = 14
	mySprite.y = 0
	-- Scale down the sprite to half its size
	mySprite.xScale = .5; mySprite.yScale = .5;
	mySprite.alpha = 1
	numberG:insert(mySprite)
	numberG.hourglass = mySprite
	
	-- move loader
	
	transition.cancel(loaderTrans)
	loaderTrans = transition.to(loadG.bar, {time=250, xScale=.70, onComplete=buildBoy})
	
end

-- All sprited, not scaled yet
buildBoy = function()

	--- boy animations
	
	local boyG = display.newGroup()
	gameG:insert(boyG)
	gameG.boy = boyG
	
	--think
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/think_up.png", 220, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 10)
	--sprite.add( mySet, "go", 1, 10, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	local sheetData = { width=220, height=290, numFrames=10}
	local mySheet = graphics.newImageSheet( "graphics/boy/think_up.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 10, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	mySprite.x = 0
	mySprite.y = 0
	mySprite.alpha = 1
	boyG:insert(mySprite)
	boyG.thinkUp = mySprite
	
	
	
	--think down
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/think_down.png", 220, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 10)
	--sprite.add( mySet, "go", 1, 10, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	local sheetData = { width=220, height=290, numFrames=10}
	local mySheet = graphics.newImageSheet( "graphics/boy/think_down.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 10, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	mySprite.x = 0
	mySprite.y = 0
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.thinkDown = mySprite
	
	
	
	
	--think loop
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/think_loop.png", 160, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 13)
	--sprite.add( mySet, "go", 1, 13, 700, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	local sheetData = { width=160, height=290, numFrames=13}
	local mySheet = graphics.newImageSheet( "graphics/boy/think_loop.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 13, --total number of frames to animate consecutively before stopping or looping
			time = 700,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
		
	mySprite.x = 23
	mySprite.y = 2
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.thinkLoop = mySprite
	
	--happy up
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/happy_up.png", 170, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 8)
	--sprite.add( mySet, "go", 1, 8, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	local sheetData = { width=170, height=290, numFrames=8}
	local mySheet = graphics.newImageSheet( "graphics/boy/happy_up.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 8, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
			
	mySprite.x = 23
	mySprite.y = -2
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.happyUp = mySprite
	
	-- happy down
	
	
	local sheetData = { width=170, height=290, numFrames=8}
	local mySheet = graphics.newImageSheet( "graphics/boy/happy_down.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 8, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/happy_down.png", 170, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 8)
	--sprite.add( mySet, "go", 1, 8, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	mySprite.x = 23
	mySprite.y = -2
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.happyDown = mySprite
	
	-- happy loop
	
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/happy_loop.png", 170, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 12)
	--sprite.add( mySet, "go", 1, 12, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	
	local sheetData = { width=170, height=290, numFrames=12}
	local mySheet = graphics.newImageSheet( "graphics/boy/happy_loop.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
		    start = 1, --starting frame index
			count = 12, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	mySprite.x = 23
	mySprite.y = -2
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.happyLoop = mySprite

	--sad down
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/sad_down.png", 180, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 10)
	--sprite.add( mySet, "go", 1, 10, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	local sheetData = { width=180, height=290, numFrames=10}
	local mySheet = graphics.newImageSheet( "graphics/boy/sad_down.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	        start = 1, --starting frame index
			count = 10, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	
	mySprite.x = 22
	mySprite.y = 0
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.sadDown = mySprite
	
	
	-- sad up
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/sad_up.png", 180, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 10)
	--sprite.add( mySet, "go", 1, 10, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	
	local sheetData = { width=180, height=290, numFrames=10}
	local mySheet = graphics.newImageSheet( "graphics/boy/sad_up.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	    start = 1, --starting frame index
			count = 10, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	
	
	mySprite.x = 22
	mySprite.y = 0
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.sadUp = mySprite
	
	-- sad loop
	
	--local mySpriteSheet = sprite.newSpriteSheet( "graphics/boy/sad_loop.png", 160, 290 )
	--local mySet   = sprite.newSpriteSet(mySpriteSheet, 1, 11)
	--sprite.add( mySet, "go", 1, 11, 500, 1 )
	--local mySprite = sprite.newSprite( mySet )
	
	
	local sheetData = { width=160, height=290, numFrames=11}
	local mySheet = graphics.newImageSheet( "graphics/boy/sad_loop.png", sheetData )
	
	local sequenceData   =  { 
		{
			name = "go",
	 	   start = 1, --starting frame index
			count = 11, --total number of frames to animate consecutively before stopping or looping
			time = 500,
			loopCount = 1
		}
	}
			
	local mySprite = display.newSprite( mySheet, sequenceData );
	
	
	mySprite.x = 22
	mySprite.y = 0
	mySprite.alpha = 0
	boyG:insert(mySprite)
	boyG.sadLoop = mySprite
	
	--INIT
	
	boyG.alpha = 0
	boyG.x = 100
	boyG.y = 450
	
	boyAnims = {boyG.thinkUp, boyG.thinkDown, boyG.thinkLoop, 
				boyG.happyUp, boyG.happyDown, boyG.happyLoop,
				boyG.sadUp, boyG.sadDown, boyG.sadLoop}
				
	-- move loader

	transition.cancel(loaderTrans)
	loaderTrans = transition.to(loadG.bar, {time=250, xScale=.85, onComplete=buildOverlay})
	
end

buildOverlay = function()

	--local black = display.newRect(0, 0, 512, 384)
	--black:setFillColor(0,0,0, 0.58)
	--overG:insert(black)
	--overG.black = black
	--black.alpha = 0;
	
	local scrollG = display.newGroup()
	scrollG.x = 256
	scrollG.y = 600
	overG:insert(scrollG)
	overG.scrollG = scrollG
	
	local scroll = display.newImageRect("graphics/shell/scroll.png", 486, 246)
	scroll.x = 0
	scroll.y = 0
	scrollG:insert(scroll)
	
	--------------------------- help
	
	local helpG = display.newGroup()
	scrollG:insert(helpG)
	scrollG.helpG = helpG
	helpG.isVisible = false
	
	local img = display.newImageRect("graphics/help/learn.png", 322, 147)
	img.x = 0
	img.y = -15
	img.alpha = 0
	helpG:insert(img)
	helpG.learn = img
	
	local img = display.newImageRect("graphics/help/challenge.png", 323, 147)
	img.x = 0
	img.y = -15
	img.alpha = 0
	helpG:insert(img)
	helpG.challenge = img
	
	local img = display.newImageRect("graphics/help/intro.png", 322, 157)
	img.x = 0
	img.y = 0
	img.alpha = 0
	helpG:insert(img)
	helpG.intro = img
	
	--------------------------- credits
	
	local creditsG = display.newGroup()
	scrollG:insert(creditsG)
	scrollG.creditsG = creditsG
	creditsG.isVisible = false
	
	--text
	
	-- local txtField = display.newText( "Music by:", 0, 0, "Turtles", 42 )
	-- txtField.x = 0	
	-- txtField.y = -65
	-- txtField:setFillColor(80,42,23)
	-- -- Scale down it .5
	-- txtField.xScale = .5; txtField.yScale = .5;
	-- creditsG:insert(txtField)
	-- creditsG.settingText1 = txtField
	
	-- local txtField = display.newText( "Ganesan (Ram) Ramachandran", 0, 0, "Turtles", 42 )
	-- txtField.x = 0	
	-- txtField.y = -40
	-- txtField:setFillColor(0,204,51)
	-- -- Scale down it .5
	-- txtField.xScale = .5; txtField.yScale = .5;
	-- creditsG:insert(txtField)
	-- creditsG.settingText2 = txtField
	
	
	-- local txtField = display.newText( "muZk.ram@gmail.com", 0, 0, "Turtles", 42 )
	-- txtField.x = 0	
	-- txtField.y = -20
	-- txtField:setFillColor(0,204,51)
	-- -- Scale down it .5
	-- txtField.xScale = .5; txtField.yScale = .5;
	-- creditsG:insert(txtField)
	-- creditsG.settingText5 = txtField
	
	local txtField = display.newText( "Design, Animation and Engineering:", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -65
	txtField:setFillColor(29/255,12/255,2/255)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	creditsG:insert(txtField)
	creditsG.settingText3 = txtField
	
	local txtField = display.newText( "Taal Media", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -35
	txtField:setFillColor(53/255,137/255,31/255)
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	creditsG:insert(txtField)
	creditsG.settingText4 = txtField
	
	
	--------------------------- settings
	
	local settingsG = display.newGroup()
	scrollG:insert(settingsG)
	scrollG.settingsG = settingsG
	settingsG.isVisible = false
	
	--learn button
	
	local btnG = display.newGroup()
	btnG.x = 0
	btnG.y = 10
	btnG.id = "learn"
	settingsG:insert(btnG)
	settingsG.learn = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "LEARN", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale down it .5
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onOverlayTouch)
	
	--challenge button
	
	local btnG = display.newGroup()
	btnG.x = 0
	btnG.y = 50
	btnG.id = "challenge"
	settingsG:insert(btnG)
	settingsG.challenge = btnG
	btnG.alpha = .5
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "CHALLENGE", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onOverlayTouch)
	
	-- If it is challenge mode, then
	-- toggle the learn/challenge 
	-- to challenge	
	if(gameMode == "challenge") then
		toggleOverlayMenu("learn")
	end
	
	--replay button
	local btnG = display.newGroup()
	btnG.x = 0
	btnG.y = 10
	btnG.isVisible = false
	btnG.id = "replay"
	settingsG:insert(btnG)
	settingsG.replay = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "RESTART", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onOverlayTouch)
	
	--exit button
	
	local btnG = display.newGroup()
	btnG.x = 0
	btnG.y = 50
	btnG.id = "exit"
	settingsG:insert(btnG)
	settingsG.exit = btnG
	btnG.isVisible = false
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "MAIN MENU", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;	
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onOverlayTouch)
	
	--ok button
	
	local btnG = display.newGroup()
	btnG.x = 185
	btnG.y = 45
	btnG.id = "ok"
	scrollG:insert(btnG)
	scrollG.ok = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_ok.png", 53, 48)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "OK", 0, 0, "Turtles", 42 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(255,204,51)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onOverlayTouch)
	
	--audio button
	
	local btnG = display.newGroup()
	btnG.x = -185
	btnG.y = 45
	btnG.id = "sound"
	settingsG:insert(btnG)
	scrollG.sound = sound
	
	local sound = display.newImageRect("graphics/shell/sound_on.png", 57, 57)
	sound.x = 0
	sound.y = 0
	sound.xScale = .8
	sound.yScale = .8
	btnG:insert(sound)
	btnG.on = sound
	
	local sound = display.newImageRect("graphics/shell/sound_off.png", 57, 57)
	sound.x = 0
	sound.y = 0
	sound.alpha = 0
	sound.xScale = .8
	sound.yScale = .8
	btnG:insert(sound)
	btnG.off = sound
	
	local txtField = display.newText( "MUTE", 0, 0, "Turtles", 36 )
	txtField.x = 0
	txtField.y = -37.5
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	btnG:insert(txtField)
	btnG.mute = txtField
	
	local txtField = display.newText( "UNMUTE", 0, 0, "Turtles", 36 )
	txtField.x = 0
	txtField.y = -37.5
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtField.alpha = 0
	btnG:insert(txtField)
	btnG.unmute = txtField
	
	btnG:addEventListener("touch", onOverlayTouch)
	
	-- Use the global boolean variable audioOn
	-- to see if mute or unmute
	if(not audioOn) then
		btnG.unmute.alpha = 1
		btnG.mute.alpha = 0
		btnG.off.alpha = 1
		btnG.on.alpha = 0;
	end
	
	
	
	--- text blocks
	
	local txtField = display.newText( "SELECT YOUR GAME MODE:", 0, 0, "Turtles", 42 )
	txtField.x = 1	
	txtField.y = -55
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	settingsG:insert(txtField)
	settingsG.settingText = txtField
	
	local txtField = display.newText( "LEAVING SO SOON?", 0, 0, "Turtles", 42 )
	txtField.x = 1	
	txtField.y = -55
	-- Scale it down to half the size
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	settingsG:insert(txtField)
	settingsG.gameText = txtField
	txtField.isVisible = false
	
	-- move loader
	
	transition.cancel(loaderTrans)
	loaderTrans = transition.to(loadG.bar, {time=250, xScale=1, onComplete=startMenu})
	
end

-------------------------- MENU EVENTS

startMenu = function()
	
	loopIntro()
	local myclosure = function() return loopIntro() end
	artTimer = timer.performWithDelay(2000, myclosure, 0)
	
	--menuG.boy.head:prepare("go")
	menuG.boy.head:play()
	
	startIntroLoop()
	
	--- animate loader off
	if(loadG) then
		transition.to(loadG.boyG,  {delay=0200, time=500, y=170.5-25, alpha=0, transition=easing.outQuad})
		transition.to(loadG.text,  {delay=0400, time=500, y=253-25, alpha=0, transition=easing.outQuad})
	end
	transition.to(menuG.bg2,   {delay=0200, time=1000, alpha=1, transition=easing.outQuad})
	
	--- animate intro on
	
	transition.to(menuG.artG, {delay=1000, time=1500, y=-12, transition=easing.outQuad})
	
	transition.to(menuG.title1, {delay=2200, time=500, y=57, transition=easing.outQuad})
	transition.to(menuG.title2, {delay=2400, time=500, y=57, transition=easing.outQuad})
	transition.to(menuG.title3, {delay=2600, time=500, y=57, transition=easing.outQuad})
	
	transition.to(menuG.help,     {delay=3200, time=500, y=360, transition=easing.outQuad})
	transition.to(menuG.credits,  {delay=3400, time=500, y=360, transition=easing.outQuad})
	transition.to(menuG.settings, {delay=3600, time=500, y=360, transition=easing.outQuad})
	
	transition.to(menuG.play, {delay=3000, time=1000, y=270, transition=easing.outQuad})
	transition.to(menuG.boy,  {delay=3500, time=1000, alpha=1, transition=easing.outQuad})
	transition.to(menuG.hand, {delay=3500, time=1000, alpha=1, transition=easing.outQuad, onComplete=unpauseGame})
		
end

resumeMenu = function() 
		
	loopIntro()
	local myclosure = function() return loopIntro() end
	artTimer = timer.performWithDelay(2000, myclosure, 0)
	
	--menuG.boy.head:prepare("go")
	menuG.boy.head:play()
	
	startIntroLoop()
	
	transition.to(menuG.title1, {delay=500, time=500, y=57, transition=easing.outQuad})
	transition.to(menuG.title2, {delay=700, time=500, y=57, transition=easing.outQuad})
	transition.to(menuG.title3, {delay=900, time=500, y=57, transition=easing.outQuad, onComplete=unpauseGame})
	
end


loopIntro = function()
	
	-- What is up with this array?
	introTrans = {}
	
	-- Why do you overwrite this array info? 
	introTrans[1] = transition.to(menuG.artG[2], {delay=0000, time=1000, x=menuG.artG[2].x - 8})
	introTrans[1] = transition.to(menuG.artG[2], {delay=1000, time=1000, x=402.5})
	
	introTrans[2] = transition.to(menuG.artG[3], {delay=0000, time=1000, y=menuG.artG[3].y - 6})
	introTrans[2] = transition.to(menuG.artG[3], {delay=1000, time=1000, y=213.5})
	
	introTrans[3] = transition.to(menuG.artG[4], {delay=0000, time=1000, rotation=menuG.artG[4].rotation - 12.5})
	introTrans[3] = transition.to(menuG.artG[4], {delay=1000, time=1000, rotation=0})
	
	introTrans[4] = transition.to(menuG.artG[5], {delay=0000, time=1000, xScale=1.08, yScale=1.08})
	introTrans[4] = transition.to(menuG.artG[5], {delay=1000, time=1000, xScale=1, yScale=1})
	
	introTrans[5] = transition.to(menuG.artG[6], {delay=0000, time=1000, x=menuG.artG[6].x + 12, y=menuG.artG[6].y - 5})
	introTrans[5] = transition.to(menuG.artG[6], {delay=1000, time=1000, x=116, y=246})
	
	introTrans[6] = transition.to(menuG.artG[7], {delay=0000, time=1000, xScale=1.11, yScale=1.11})
	introTrans[6] = transition.to(menuG.artG[7], {delay=1000, time=1000, xScale=1, yScale=1})
	
end

hideMenu = function()
	
	transition.to(menuG.title1, {delay=0000, time=300, y=57-100, transition=easing.inQuad})
	transition.to(menuG.title2, {delay=0100, time=300, y=57-100, transition=easing.inQuad})
	transition.to(menuG.title3, {delay=0200, time=300, y=57-100, transition=easing.inQuad})
		
	transition.to(menuG.artG, {delay=100, time=1000, y=-400, transition=easing.inQuad})
	transition.to(menuG.play, {time=1000, y=425, transition=easing.inQuad})
	transition.to(menuG.boy,  {time=500, alpha=0, transition=easing.inQuad})
	transition.to(menuG.hand,  {time=500, alpha=0, transition=easing.inQuad})
	
	local myclosure = function() return showGame() end
	timer.performWithDelay(1000, myclosure, 1)
	
	--_G.isPaused = true
	pauseGame()
	
	----
	overG.scrollG.settingsG.learn.isVisible = false
	overG.scrollG.settingsG.challenge.isVisible = false
	
	overG.scrollG.settingsG.replay.isVisible = true
	overG.scrollG.settingsG.exit.isVisible = true
	
	--
	overG.scrollG.settingsG.settingText.isVisible = false
	overG.scrollG.settingsG.gameText.isVisible = true
	
end

onShellMenuTouch = function(e)
	
	if(_G.isPaused)then
		return
	end

	if(e.phase == "began")then
		
		if(e.target.id == "play")then
			
			hideMenu()
			fadeOutMusic()
			
		end
		
		if(e.target.id == "help")then
			
			showOverlay("help")
			_G.isPaused = true
			
		end
		
		if(e.target.id == "settings")then
		
			showOverlay("settings")
			_G.isPaused = true
			
		end
		
		if(e.target.id == "credits")then
			
			showOverlay("credits")
			_G.isPaused = true
			
		end
		
		playAudio("click")
		
	end
	
end

-------------------------- OVERLAY EVENTS
showOverlay = function(type)
	
	overG.scrollG.settingsG.isVisible = false
	overG.scrollG.creditsG.isVisible = false
	overG.scrollG.helpG.isVisible = false

	if(type == "settings")then
	
		overG.scrollG.settingsG.isVisible = true
		
	end
	
	if(type == "help")then
	
		overG.scrollG.helpG.isVisible = true
		
		overG.scrollG.helpG.intro.alpha = 0
		overG.scrollG.helpG.learn.alpha = 0
		overG.scrollG.helpG.challenge.alpha = 0
		
		if(menuG.boy.alpha == 1)then
			
			overG.scrollG.helpG.intro.alpha = 1
			
		else
		
			if(_G.gameMode == "learn")then
				overG.scrollG.helpG.learn.alpha = 1
			else
				overG.scrollG.helpG.challenge.alpha = 1
			end
			
		end
		
		
	end
	
	if(type == "credits")then
	
		overG.scrollG.creditsG.isVisible = true
		
	end
	
	transition.to(overG.scrollG, {delay=0400, time=700, y=200, transition=easing.outQuad})
	--transition.to(overG.black,   {delay=0000, time=500, alpha=1, transition=easing.outQuad})
	
end

-- Public function gets called from main sometimes
function toggleOverlayMenu(type)

	if(type == "learn")then
		
		overG.scrollG.settingsG.learn.alpha = .5
		overG.scrollG.settingsG.challenge.alpha = 1
		
	elseif(type == "challenge")then
		
		overG.scrollG.settingsG.learn.alpha = 1
		overG.scrollG.settingsG.challenge.alpha = .5
		
	end
	
end

onOverlayTouch = function(e)

	if(overG.scrollG.y ~= 200)then
		return
	end
	
	if(e.phase == "began")then
	
		playAudio("click")
	
		if(e.target.id == "learn")then
			
			if(_G.gameMode == "challenge")then
				
				_G.gameMode = "learn"
				saveGameMode(_G.gameMode)
				-- Set starting level to 1
				saveLevelProgress(1)
				transition.to(overG.scrollG.settingsG.learn,     {time=250, alpha=1, transition=easing.outQuad})
				transition.to(overG.scrollG.settingsG.challenge, {time=250, alpha=.5, transition=easing.outQuad})
				
			end
			
		end
		
		if(e.target.id == "challenge")then						
			if(_G.gameMode == "learn")then
				if(not isPremium) then
					-- Temporary handler
					local function onComplete( event )
						if "clicked" == event.action then
							local i = event.index
							if 1 == i then
									unlockPremiumClosure()
									print("Buying..")
	
							elseif 2 == i then
									cancelled = true;
							end
						end
					end
					
					native.showAlert("Fremium Version","Challenge mode is only available in premium version. Would you like to update to it?", {"Buy it", "Not now"}, onComplete)
					return true;
				end
			
				
				_G.gameMode = "challenge"
				saveGameMode(_G.gameMode)
				transition.to(overG.scrollG.settingsG.learn,     {time=250, alpha=.5, transition=easing.outQuad})
				transition.to(overG.scrollG.settingsG.challenge, {time=250, alpha=1, transition=easing.outQuad})
				
			end
			
		end
		
		if(e.target.id == "replay")then
			
			transition.to(overG.scrollG, {delay=0000, time=700, y=600, transition=easing.inQuad})
			--transition.to(overG.black,   {delay=400, time=500, alpha=0, transition=easing.inQuad})
			
			fadeOutMusic()
			engine.resetCurrentGame("replay")
			
		end
		
		if(e.target.id == "exit")then
			
			transition.to(overG.scrollG, {delay=0000, time=700, y=600, transition=easing.inQuad})
			--transition.to(overG.black,   {delay=400, time=500, alpha=0, transition=easing.inQuad})
			
			fadeOutMusic()
			engine.resetCurrentGame("menu")
			
		end
		
		if(e.target.id == "ok")then
			
			closeOverlay()
			
		end
		
		if(e.target.id == "sound")then
		
			toggleAudioOnOff()
			if(e.target.on.alpha == 1)then
				e.target.on.alpha = 0
				e.target.off.alpha = 1
				e.target.mute.alpha = 0
				e.target.unmute.alpha = 1
			else
				e.target.on.alpha = 1
				e.target.off.alpha = 0
				e.target.mute.alpha = 1
				e.target.unmute.alpha = 0
			end
			
		end
		
	end
	
end

closeOverlay = function()
	
	transition.to(overG.scrollG, {delay=0000, time=700, y=600, transition=easing.inQuad})
	--transition.to(overG.black,   {delay=400, time=500, alpha=0, transition=easing.inQuad, onComplete=unpauseShell})
	timer.performWithDelay( 400, unpauseShell )
	
end

unpauseShell = function()
	
	if(_G.firstHelp)then
		
		_G.firstHelp = false
		engine.startCardGame()
		
	else
			
		--_G.isPaused = false	
		unpauseGame()		
		-- If in challenge mode, start pouring sand again
		if(_G.gameMode == "challenge") then
			pourSand("start")
		end	
	end
	
end

function pauseGame()
	_G.isPaused = true
	transition.to(menuG.credits,  {time=500, alpha = .2, transition=easing.outQuad})
	transition.to(menuG.help,  {time=500, alpha = .2, transition=easing.outQuad})
	transition.to(menuG.settings,  {time=500, alpha = .2, transition=easing.outQuad})
	--transition.to(menuG.play,  {time=500, alpha = .7, transition=easing.outQuad})
end

function unpauseGame()
	_G.isPaused = false
	transition.to(menuG.credits,  {time=500, alpha = 1, transition=easing.outQuad})
	transition.to(menuG.help,  {time=500, alpha = 1, transition=easing.outQuad})
	transition.to(menuG.settings,  {time=500, alpha = 1, transition=easing.outQuad})
	--transition.to(menuG.play,  {time=500, alpha = 1, transition=easing.outQuad})
end

-------------------------- GAME EVENTS
-- public function that gets called from the engine
function addEngine(myGroup)

	gameG:insert(myGroup)
	
end

showGame = function()
	
	timer.cancel(artTimer)
	menuG.boy.head:pause()
	
	for i=1, #introTrans do
		transition.cancel(introTrans[i])
	end

	transition.to(gameG.bg1,   {delay=0000, time=1000, y=194, transition=easing.outQuad})
	transition.to(gameG.timer, {delay=0600, time=1000, y=26, transition=easing.outQuad})
	
	if(_G.gameMode == "learn")then
		gameG.timer.numbers.alpha = 0
	elseif(_G.gameMode == "challenge")then
		gameG.timer.numbers.alpha = 1
	end
	
	local myclosure = function() return startGame() end
	timer.performWithDelay(2000, myclosure, 1)
	
end

startGame = function()
	
	local savedLevel = tonumber(getCurrentLevel())
	-- Make the levels go in the circle
	if(savedLevel == 0 or savedLevel > 7) then
		savedLevel = 1
		saveLevelProgress(savedLevel)
	end
	
	-- Set this to any level you want to test.
	--savedLevel = 7
	engine.buildEngine(savedLevel)
	
end

-- public function, get's called from the engine
function backToMenu()
	
	transition.to(gameG.timer, {delay=0000, time=1000, y=-100, transition=easing.inQuad})
	transition.to(gameG.bg1,   {delay=0600, time=1000, y=-400, transition=easing.inQuad})
	
	transition.to(menuG.artG, {delay=1600, time=1000, y=-12, transition=easing.outQuad})
	transition.to(menuG.play, {delay=2000, time=1000, y=270, transition=easing.outQuad})
	transition.to(menuG.boy,  {delay=2500, time=500, alpha=1, transition=easing.outQuad})
	transition.to(menuG.hand,  {delay=2500, time=500, alpha=1, transition=easing.outQuad})
	
	local myclosure = function() return resumeMenu() end
	timer.performWithDelay(2500, myclosure, 1)
	
	overG.scrollG.settingsG.learn.isVisible = true
	overG.scrollG.settingsG.challenge.isVisible = true
	
	overG.scrollG.settingsG.replay.isVisible = false
	overG.scrollG.settingsG.exit.isVisible = false
	
	overG.scrollG.settingsG.settingText.isVisible = true
	overG.scrollG.settingsG.gameText.isVisible = false
	
end

-- public function, get's called from the engine
function updateTimer(type, numString)

	if(type == "best")then
		
		gameG.timer.numbers.bestTime.text = numString
		gameG.timer.numbers.bestTime.anchorX = 0.5;
		gameG.timer.numbers.bestTime.anchorY = 0.5;

		gameG.timer.numbers.bestTime.x = -142
		
	elseif(type == "new")then
	
		--print("UPDATE NEW TIME: " .. numString)
		gameG.timer.numbers.newTime.text = numString
		gameG.timer.numbers.newTime.anchorX = 0.5;
		gameG.timer.numbers.newTime.anchorY = 0.5;

		gameG.timer.numbers.newTime.x = 160
		
	end
	
end

-- public function, get's called from the engine
function showTimer()

	transition.to(gameG.timer.numbers, {time=500, alpha=1, transition=easing.outQuad})

end

-- public function, get's called from the engine
function showHelp()
	showOverlay("help")
	_G.isPaused = true
end

-- public function, get's called from the engine
function pourSand(type)

	
	if(type == "start")then
		if(_G.isPaused) then
			return;
		end
		gameG.timer.numbers.hourglass.rotation = 0
		--gameG.timer.numbers.hourglass:prepare("go")
		gameG.timer.numbers.hourglass:play()
		local myclosure = function() return pourSand("start") end
		sandTrans = transition.to(gameG.timer.numbers.hourglass, {
			delay=2200, 
			rotation=180, 
			transition=easing.inOutQuad,
			onComplete=myclosure})
		
	end
	
	if(type == "stop")then

		gameG.timer.numbers.hourglass.rotation = 0
		--gameG.timer.numbers.hourglass:prepare("go")
		if(sandTrans)then
			transition.cancel(sandTrans)
		end
		
	end

	
end



-------------------------- BOY EVENTS

-- public function, get's called from the engine
function moveBoy(type)

	
	--print("moveBoy: " .. type)
	
	for i=1, #boyTimers do
		-- Cancel old timers
		timer.cancel(boyTimers[i])
		-- Nil out old references
		boyTimers[i] = nil
	end
	boyTimers = {}

	if(type == "think")then
		
		--print("THINK ANIMATION")
	
		local myclosure = function() return animateBoy("think_up") end
		timer1 = timer.performWithDelay(0000, myclosure, 1)
		
		local myclosure = function() return animateBoy("think_loop") end
		timer2 = timer.performWithDelay(0600, myclosure, 1)
		
		local myclosure = function() return animateBoy("think_loop") end
		timer3 = timer.performWithDelay(1400, myclosure, 1)
		
		local myclosure = function() return animateBoy("think_loop") end
		timer4 = timer.performWithDelay(2200, myclosure, 1)
		
		local myclosure = function() return animateBoy("think_down") end
		timer5 = timer.performWithDelay(3000, myclosure, 1)
		
		---loop it
		
		local myclosure = function() return moveBoy("think") end
		timer6 = timer.performWithDelay(3900, myclosure, 1)
		
		---store it
		
		boyTimers={timer1, timer2, timer3, timer4, timer5, timer6}
		
	end
	
	if(type == "sad")then
	
		--print("SAD ANIMATION")
	
		local myclosure = function() return animateBoy("sad_down") end
		timer1 = timer.performWithDelay(0000, myclosure, 1)
		
		local myclosure = function() return animateBoy("sad_loop") end
		timer2 = timer.performWithDelay(0600, myclosure, 1)
		
		local myclosure = function() return animateBoy("sad_loop") end
		timer3 = timer.performWithDelay(1200, myclosure, 1)
		
		local myclosure = function() return animateBoy("sad_up") end
		timer4 = timer.performWithDelay(1800, myclosure, 1)
		
		---loop it
		
		local myclosure = function() return moveBoy("think") end
		timer5 = timer.performWithDelay(2600, myclosure, 1)
		
		---store it
		
		boyTimers={timer1, timer2, timer3, timer4, timer5}
		
	end
	
	if(type == "happy")then
	
		--print("HAPPY ANIMATION")
	
		local myclosure = function() return animateBoy("happy_up") end
		timer1 = timer.performWithDelay(0000, myclosure, 1)
		
		local myclosure = function() return animateBoy("happy_loop") end
		timer2 = timer.performWithDelay(0600, myclosure, 1)
		
		local myclosure = function() return animateBoy("happy_down") end
		timer3 = timer.performWithDelay(2100, myclosure, 1)
		
		---loop it
		
		local myclosure = function() return moveBoy("think") end
		timer4 = timer.performWithDelay(3000, myclosure, 1)
		
		---store it
		
		boyTimers={timer1, timer2, timer3, timer4}
		
	end
	
	if(type == "stop")then
		animateBoy("stop")
	end
	
end

animateBoy = function(type)
	
	for i=1, #boyAnims do
	
		boyAnims[i]:pause()
		boyAnims[i].alpha = 0
		
	end
	
	if(type == "think_up")then
		
		--gameG.boy.thinkUp:prepare("go")
		gameG.boy.thinkUp:play()
		gameG.boy.thinkUp.alpha = 1
		
	end
	
	if(type == "think_down")then
		
		--gameG.boy.thinkDown:prepare("go")
		gameG.boy.thinkDown:play()
		gameG.boy.thinkDown.alpha = 1
		
	end
	
	if(type == "think_loop")then
		
		--gameG.boy.thinkLoop:prepare("go")
		gameG.boy.thinkLoop:play()
		gameG.boy.thinkLoop.alpha = 1
		
	end
	
	if(type == "sad_down")then
		
		--gameG.boy.sadDown:prepare("go")
		gameG.boy.sadDown:play()
		gameG.boy.sadDown.alpha = 1
		
	end
	
	if(type == "sad_up")then
		
		--gameG.boy.sadUp:prepare("go")
		gameG.boy.sadUp:play()
		gameG.boy.sadUp.alpha = 1
		
	end
	
	if(type == "sad_loop")then
	
		--gameG.boy.sadLoop:prepare("go")
		gameG.boy.sadLoop:play()
		gameG.boy.sadLoop.alpha = 1
		
	end
	
	if(type == "happy_up")then
		
		--gameG.boy.happyUp:prepare("go")
		gameG.boy.happyUp:play()
		gameG.boy.happyUp.alpha = 1
		
	end
	
	if(type == "happy_down")then
		
		--gameG.boy.happyDown:prepare("go")
		gameG.boy.happyDown:play()
		gameG.boy.happyDown.alpha = 1
		
	end
	
	if(type == "happy_loop")then
		
		--gameG.boy.happyLoop:prepare("go")
		gameG.boy.happyLoop:play()
		gameG.boy.happyLoop.alpha = 1
		
	end
	
	if(type == "stop")then
		
		--gameG.boy.thinkUp:prepare("go")
		gameG.boy.thinkUp.alpha = 1
		
	end
	
end

-- public function, get's called from the engine
function hideBoy()

	transition.to(gameG.boy, {time=500, alpha=0, transition=easing.outQuad})
	
	local myclosure = function() return moveBoy("stop") end
	timer.performWithDelay(600, myclosure, 1)

end

-- public function, get's called from the engine
function showBoy()

	if(_G.gameMode == "learn")then
		
		gameG.boy.x = 55
		gameG.boy.y = 200
		gameG.boy.xScale = .6
		gameG.boy.yScale = .6 
		
	elseif(_G.gameMode == "challenge")then
		
		gameG.boy.x = 90
		gameG.boy.y = 200
		gameG.boy.xScale = 0.6
		gameG.boy.yScale = 0.6
		
	end
	transition.to(gameG.boy, {time=500, alpha=1, transition=easing.outQuad})

end

-------------------------- Memory management
local function onMemoryWarning( event )
	print("Memory warning issued")
	-- Handling a memory warning
	
	-- Remove loading group
	if(loadG) then
		display.remove(loadG)
		loadG = nil
	end
end

Runtime:addEventListener( "memoryWarning", onMemoryWarning )

-------------------------- CLEAN UP

function killArt()
	
end


