application =
{
        content =
        {
        		width = 384, -- 768
                height = 512, -- 1024
                scale = "zoomStretch",
                
                imageSuffix =
				{
					--["-x15"] = 1.5,		-- A good scale for Droid, Nexus One, etc.
					["-x2"] = 2.0,		-- A good scale for iPhone 4 and iPad
				},
        },
}