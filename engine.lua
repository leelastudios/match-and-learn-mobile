
module(..., package.seeall)

-------------------------- IMPORTS & SETTINGS


-------------------------- GLOBAL VARIABLES

local engineG

-- Group that holds all the cards
local cardGroup 

local firstCard = nil
local secondCard = nil
local gameStatus = "playing"
local gameType = nil

-- Constant number for the number of card pairds
local TOTAL_GAME_PAIRS = 0
local totalMatches = 0
local curLevel = 0
local challengeTimer
local curTime = 0

-- Declare local functions
local buildCards
local buildRewards
local enableCards
local countTimer

local showCard, hideCard, removeCard
local evaluateCards

local playReward, hideReward
local resumeEngine, resetGame

local setToReward, setChallengeMode

local returnToMenu

local tableShuffle, groupShuffle

-- Local card event listener
local onCardTouch, onRewardTouch

--local killEngine


-------------------------- CONSTRUCTION
-- Main function called from other shell module
function buildEngine(myLevel)
	
	-- Save this level as current level
	saveLevelProgress(myLevel)
	-- Create the engine group
	engineG = display.newGroup()
	--push engine group back to shell
	shell.addEngine(engineG)
	
	-- Reset the game variables
	gameType = _G.gameMode
	curLevel = myLevel
	

	
	---build new array for level list
	local finalList = {}
	
	--choose data to send
	if(_G.gameMode == "learn")then
	
		--send level data
		finalList = data.allData[curLevel]
		
	elseif(_G.gameMode == "challenge")then
		
		--find ammount from each level
		--this will grab # of cards from each level
		--and the remainder from the final level
		local levelAmount  = math.floor(6 / (#data.allData - 1))
		print("Level amount: " .. levelAmount)
		for i=1, (#data.allData-1) do
			
			local levelData = data.allData[i]
			tableShuffle(levelData)
		
			for j=1, levelAmount do
				finalList[#finalList+1] = levelData[j]
			end
			
		end
		---[[
		--insert last level to fill remainder
		local levelData = data.allData[#data.allData]
		tableShuffle(levelData)
		local remainder = 6 - #finalList
		for i=1, remainder do
		
			finalList[#finalList+1] = levelData[i]
			
		end
		--]]
		--reset time
		curTime = 0
		shell.updateTimer("new", formatTime("MY TIME: ", curTime) )

		shell.showTimer()

		
		--check final list
		print("CHALLENGE LIST")
		print("*************")
		for i=1, #finalList do
		
			print(finalList[i][1] .. " :: " .. finalList[i][2])
			
		end
		print("*************")
		
	end
	
	buildCards( finalList )
	buildRewards()
	
end

--------------------------- LOCAL CONSTRUCTION
buildCards = function(myLevel)
	-- Create a group to hold all the cards
	cardGroup = display.newGroup()
	engineG:insert(cardGroup)
	
	-- Get the total number of card pairs
	-- depending which game mode it is
	local totalCards
	if(gameType == "learn")then
		totalCards = 4
	elseif(gameType == "challenge")then
		totalCards = 6
	end
		
	--shuffle table (per level)
	local levelData = myLevel
	tableShuffle(levelData)
	
	
	--build image cards
	for i=1, totalCards do
	
		--card group 
		local cardG = display.newGroup()
		
		--back of card group
		local backG = display.newGroup()
		cardG:insert(backG)
		local bg = display.newImageRect("graphics/engine/card_back.png", 76, 89)
		-- Are these necessary calls?
		--bg.x = 0
		--bg.y = 0
		backG:insert(bg)
		local frame = display.newImageRect("graphics/engine/card_frame2.png", 100, 112)
		-- Are these necessary calls?

		--frame.x = 0
		--frame.y = 0
		frame.xScale = .95
		backG:insert(frame)
		local lock = display.newImageRect("graphics/engine/lock.png", 79, 79)
		-- Are these necessary calls?
		--lock.x = 0
		--lock.y = 0
		lock.xScale = .8
		lock.yScale = .8
		backG:insert(lock)
		
		--front of card group
		local frontG = display.newGroup()
		cardG:insert(frontG)
		local bg = display.newImageRect(levelData[i][1], 80, 91)
		-- Are these necessary calls?
		--bg.x = 0
		--bg.y = 0
		frontG:insert(bg)
		local frame = display.newImageRect("graphics/engine/card_frame.png", 91, 103)
		-- Are these necessary calls?
		--frame.x = 0
		--frame.y = 0
		frontG:insert(frame)
		
		--set card properties
		cardG.type = "image"
		cardG.asset = levelData[i][1]
		cardG.answer = levelData[i][2]
		cardG.front = frontG
		cardG.back = backG
		
	for k, v in pairs(levelData) do  print(tostring(k).."  "..tostring(v)) end 
		
		-- Insert 
		cardGroup:insert(cardG);
		
	end
			
	--build word cards
	for i=1, totalCards do
	
		--card group 
		local cardG = display.newGroup()
		
		--back of card group
		local backG = display.newGroup()
		cardG:insert(backG)
		local bg = display.newImageRect("graphics/engine/card_back.png", 76, 89)
		-- Are these necessary calls?
		--bg.x = 0
		--bg.y = 0
		backG:insert(bg)
		local frame = display.newImageRect("graphics/engine/card_frame2.png", 91, 103)
		-- Are these necessary calls?
		--frame.x = 0
		--frame.y = 0
		frame.xScale = .95
		backG:insert(frame)
		local key = display.newImageRect("graphics/engine/key.png", 79, 79)
		-- Are these necessary calls?
		--key.x = 0
		--key.y = 0
		key.xScale = .8
		key.yScale = .8
		backG:insert(key)
		
		--front of card group
		local frontG = display.newGroup()
		cardG:insert(frontG)
		local bg = display.newImageRect(levelData[i][2], 76, 89)
		-- Are these necessary calls?
		--bg.x = 0
		--bg.y = 0
		frontG:insert(bg)
		local frame = display.newImageRect("graphics/engine/card_frame.png", 91, 103)
		-- Are these necessary calls?
		--frame.x = 0
		--frame.y = 0
		frontG:insert(frame)
		
		--set card properties
		cardG.type = "text"
		cardG.asset = levelData[i][2]
		cardG.answer = levelData[i][1]
		cardG.front = frontG
		cardG.back = backG
		
		-- Add this card to the card group
		cardGroup:insert(cardG)
		
	end
	
	
	
	--shuffle deck, "flip cards" and distribute, animate 
	groupShuffle(cardGroup)
	local col = 0
	local row = 0
	local delayer = 500
	
	for i=1, cardGroup.numChildren do
		
		local tempCard = cardGroup[i]
		--"flip"
		tempCard.front.xScale = .1
		tempCard.front.alpha = 0
		tempCard.status = "face-down"
	
		--distribute 
		if(gameType == "learn")then
			
			tempCard.x = 165 + (col * 95)
			tempCard.y = 134 + (row * 111)

			col = col + 1
			if(col >= 4)then
				col = 0
				row = row + 1
			end
			
		elseif(gameType == "challenge")then
			
			tempCard.xScale = .78
			tempCard.yScale = .78
			
			tempCard.x = 235 + (col * 74.5)
			tempCard.y = 100 + (row * 87)

			col = col + 1
			if(col >= 4)then
				col = 0
				row = row + 1
			end
			
		end
		
		--animate
		tempCard.alpha = 0
		tempCard.y = tempCard.y + 20
		transition.to(tempCard, {delay=delayer, time=500, alpha=1, 
									y=tempCard.y - 10, transition=easing.outQuad})
		delayer = delayer + 150
		
		if(i == cardGroup.numChildren)then
			
			local myclosure = function() return enableCards() end
			timer.performWithDelay(delayer + 600, myclosure, 1)
			
		end
		
	end

	totalMatches = 0
	TOTAL_GAME_PAIRS = cardGroup.numChildren/2
	
	--show boy
	local myclosure = function() return shell.showBoy() end
	timer.performWithDelay(1500, myclosure, 1)
	
	--level title
	
	local titleG = display.newGroup()
	titleG.x = 256
	titleG.y = 192 + 25
	titleG.alpha = 0
	engineG:insert(titleG)
	
	-- Not changed coordinates
	local level = display.newImageRect("graphics/engine/title.png", 221, 103)
	level.x = -3
	level.y = 0
	level.yScale = .7
	titleG:insert(level)
	
	local newStr = ""
	if(_G.gameMode == "learn")then
		
		newStr = data.allLevelNames[curLevel]
		
	else
	
		newStr = "CHALLENGE MODE"
		
	end
	
	-- Not changed coordinates
	local txtField = display.newText( newStr, 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -3
	-- Scale the text to half it's size
	txtField:setFillColor(29/255,12/255,2/255)
	txtField.xScale = .5; txtField.yScale = .5;
	titleG:insert(txtField)
	
	transition.to(titleG, {delay=0100, time=500, y=192, alpha=1, transition=easing.outQuad})
	transition.to(titleG, {delay=3100, time=500, y=192 + 25, alpha=0, transition=easing.inQuad})
	
end

-- Scaled
buildRewards = function()

	--local black = display.newRect(0, 0, 512, 384)
	--black:setFillColor(0,0,0, 150)
	--engineG:insert(black)
	--engineG.black = black
	--black.alpha = 0

	local rewardG = display.newGroup()
	engineG:insert(rewardG)
	engineG.rewardG = rewardG
	
	--reward panel
	
	local bg = display.newImageRect("graphics/game/bg3.png", 464, 192)
	bg.x = 256
	bg.y = 91
	bg.alpha = 1
	rewardG:insert(bg)
	
	----
	
	local txtG = display.newGroup()
	rewardG:insert(txtG)
	rewardG.learn1 = txtG
	
	local txtField = display.newText( "WAY TO GO!", 0, 0, "Turtles", 60 )
	txtField.x = 256	
	txtField.y = 95 - 13
	-- Scale the text by half
	txtField:setFillColor(29/255,12/255,2/255)
	txtField.xScale = .5; txtField.yScale = .5;

	txtG:insert(txtField)
	
	local txtField = display.newText( "REPLAY THIS ROUND OR MOVE ON TO ", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 125 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( "PLAY MORE CARDS!", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 145 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	txtG.alpha = 0
	
	-----
	
	local txtG = display.newGroup()
	rewardG:insert(txtG)
	rewardG.learn2 = txtG
	
	local txtField = display.newText( "ALL LEVELS COMPLETED!", 0, 0, "Turtles", 60 )
	txtField.x = 256	
	txtField.y = 95 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( "REPLAY THIS ROUND OR MOVE ON TO", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 125 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( "CHALLENGE MODE!", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 145 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	txtG.alpha = 0
	
	-----
	
	local txtG = display.newGroup()
	rewardG:insert(txtG)
	rewardG.challenge1 = txtG
	
	local txtField = display.newText( "NOT BAD!", 0, 0, "Turtles", 60 )
	txtField.x = 256	
	txtField.y = 95 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( "YOU MATCHED ALL THE CARDS, BUT", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 125 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( " TRY AGAIN TO BEAT YOUR BEST TIME!", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 145 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	txtG.alpha = 0
	
	-----
	
	local txtG = display.newGroup()
	rewardG:insert(txtG)
	rewardG.challenge2 = txtG
	
	local txtField = display.newText( "IT'S A NEW RECORD!", 0, 0, "Turtles", 60 )
	txtField.x = 256	
	txtField.y = 95 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( "99", 0, 0, "Turtles", 36 )
	txtField.x = 350	
	txtField.y = 125 - 13
	-- Scale the text by half
	txtField:setFillColor(0/255,204/255,51/255);
	txtField.xScale = .5; txtField.yScale = .5;

	txtG:insert(txtField)
	rewardG.timeText = txtField
	
	local txtField = display.newText( "YOU BEAT YOUR OLD TIME BY    SECONDS!", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 125 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	local txtField = display.newText( "THINK YOU CAN DO BETTER? FIND OUT!", 0, 0, "Turtles", 36 )
	txtField.x = 256	
	txtField.y = 145 - 13
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(29/255,12/255,2/255)
	txtG:insert(txtField)
	
	txtG.alpha = 0
	
	------>>>>>>>>>>
	
	rewardG.y = -250
	
	--play again
	
	local btnG = display.newGroup()
	btnG.x = 100--317
	btnG.y = 215 + 10
	btnG.alpha = 0
	btnG.id = "play again"
	engineG:insert(btnG)
	engineG.playAgain = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "TRY AGAIN", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onRewardTouch)
	
	--next level
	
	local btnG = display.newGroup()
	btnG.x = 256
	btnG.y = 215 + 10
	btnG.alpha = 0
	btnG.id = "next"
	engineG:insert(btnG)
	engineG.next = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "PLAY MORE", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5;
	txtField:setFillColor(1,0.8,0.2)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onRewardTouch)
	
	--challenge
	
	local btnG = display.newGroup()
	btnG.x = 256
	btnG.y = 215 + 10
	btnG.alpha = 0
	btnG.id = "challenge"
	engineG:insert(btnG)
	engineG.challenge = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btn_set.yScale = 1.4
	btnG:insert(btn_set)
	
	local txtField = display.newText( "CHALLENGE", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -12
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5
	txtField:setFillColor(255,204,51)
	btnG:insert(txtField)
	
	local txtField = display.newText( "MODE", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = 3
	-- Scale the text by half
	txtField.xScale = .5; txtField.yScale = .5
	txtField:setFillColor(255,204,51)
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onRewardTouch)
	
	--back to menu
	
	local btnG = display.newGroup()
	btnG.x = 414 --707
	btnG.y = 215 + 10
	btnG.alpha = 0
	btnG.id = "back"
	engineG:insert(btnG)
	engineG.back = btnG
	
	local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
	btn_set.x = 0
	btn_set.y = 0
	btnG:insert(btn_set)
	
	local txtField = display.newText( "MENU", 0, 0, "Turtles", 36 )
	txtField.x = 0	
	txtField.y = -5
	-- Scale the text by half
	txtField:setFillColor(1,0.8,0.2)
	txtField.xScale = .5; txtField.yScale = .5
	
	btnG:insert(txtField)
	
	btnG:addEventListener("touch", onRewardTouch)
	
	
	-- Invite friends button
	-- you can turn the button on/off by changing the 
	-- inviteFriendsAllowed boolean in the main.lua
	if inviteFriendsAllowed then
		local btnG = display.newGroup()
		btnG.x = 256 --707
		btnG.y = 215 + 10 + 50
		btnG.alpha = 0
		btnG.id = "invite"
		engineG:insert(btnG)
		engineG.invite = btnG
		
		local btn_set = display.newImageRect("graphics/shell/btn_med.png", 155, 36)
		btn_set.x = 0
		btn_set.y = 0
		btnG:insert(btn_set)
		
		local txtField = display.newText( "Invite Friends", 0, 0, "Turtles", 36 )
		txtField.x = 0	
		txtField.y = -5
		-- Scale the text by half
		txtField.xScale = .5; txtField.yScale = .5;
		txtField:setFillColor(255,204,51)
		btnG:insert(txtField)
		
		btnG:addEventListener("touch", onRewardTouch)
		
	end
	
end

-------------------------- ENGINE EVENTS

enableCards = function()
	
	startPlayLoop()
	
	for i=1, cardGroup.numChildren do
		local tempCard = cardGroup[i];
		if(tempCard.alpha == 1) then
			tempCard.status = "face-down"
			tempCard:addEventListener("touch", onCardTouch)
		end
	end
	
	gameStatus = "playing"
	firstCard = nil
	secondCard = nil
	
	shell.moveBoy("think")
	
	if(_G.firstPlay)then
		
		_G.firstPlay = false
		_G.firstHelp = true
		shell.showHelp()
		
	else
	
		startCardGame()
		
	end
	
end


-- Called from shell
function startCardGame()
	
	--_G.isPaused = false
	shell.unpauseGame()
	if(_G.gameMode == "challenge")then

		local myclosure = function() return countTimer() end
		challengeTimer = timer.performWithDelay(1000, myclosure, 0)
		
		shell.pourSand("start")

	end
	
end

countTimer = function()

	if(_G.isPaused)then
		return
	end
	
	curTime = curTime + 1

	shell.updateTimer("new", formatTime("MY TIME: ", curTime) )
	
end

-- Is called from shell.
function formatTime(type, myTime)

	local timeString
	
	if(myTime < 60)then
	
		if(myTime < 10)then
			timeString = type .. "00:0" .. myTime
		else
			timeString = type .. "00:" .. myTime
		end
		
		
	elseif(myTime < 3600)then
		
		local min = math.floor(myTime/60)
		local sec = myTime - (min * 60)
		
		local minStr
		local secStr
		
		if(min < 10)then
			minStr = "0" .. min
		else
			minStr = min
		end
		if(sec < 10)then
			secStr = "0" .. sec
		else
			secStr = sec
		end
		
		timeString = type .. minStr .. ":" .. secStr
		
	else
		
		timeString = type .. "TIME UP"
				
	end


	
	return timeString
	
end

onCardTouch = function(e)
	
	if(_G.isPaused)then
		return
	end
	
	if(gameStatus ~= "playing")then
		return
	end

	if(e.phase == "began")then
	
		if(e.target.status == "face-down")then
			
			playAudio("click")
			
			if(firstCard == nil)then
				firstCard = e.target
				showCard(e.target)
			else
				secondCard = e.target
				showCard(e.target)
				gameStatus = "evaluating"
				local myclosure = function() return evaluateCards() end
				timer.performWithDelay(1000, myclosure, 1)
			end
			
		end
		
	end
	
end

showCard = function(myCard)
	
	myCard.status = "face-up"
	
	transition.to(myCard.back,  {delay=0000, time=200, xScale=.1, transition=easing.inQuad})
	transition.to(myCard.back,  {delay=0200, time=000, alpha=0})
	transition.to(myCard.front, {delay=0200, time=000, alpha=1})
	transition.to(myCard.front, {delay=0200, time=200, xScale=1, transition=easing.outQuad})
	
end

hideCard = function(myCard)
	
	myCard.status = "face-down"
	
	transition.to(myCard.front,  {delay=0000, time=200, xScale=.1, transition=easing.inQuad})
	transition.to(myCard.front,  {delay=0200, time=000, alpha=0})
	transition.to(myCard.back, {delay=0200, time=000, alpha=1})
	transition.to(myCard.back, {delay=0200, time=200, xScale=1, transition=easing.outQuad})
	
end

-- Not scaled the y cord
removeCard = function(myCard)
	local function deleteCard() 	
		display.remove(myCard);
	end
	myCard.deleted = true;
	transition.to(myCard, {time=200, alpha=0, y=myCard.y + 10, transition=easing.inQuad, onComplete = deleteCard})
	
end

evaluateCards = function()
	
	local asset = firstCard.asset
	local answer = secondCard.answer
	
	if(asset == answer)then
		
		totalMatches = totalMatches + 1
		
		local myclosure = function() return removeCard(firstCard) end
		timer.performWithDelay(0000, myclosure, 1)
		
		local myclosure = function() return removeCard(secondCard) end
		timer.performWithDelay(0200, myclosure, 1)
		
		local myclosure
		if(totalMatches == TOTAL_GAME_PAIRS)then
			myclosure = function() return playReward() end
		else
			myclosure = function() return resumeEngine() end
		end
		timer.performWithDelay(0500, myclosure, 1)
		
		shell.moveBoy("happy")


		playAudio("goodCard")
		
	else
	
		local myclosure = function() return hideCard(firstCard) end
		timer.performWithDelay(0000, myclosure, 1)
		
		local myclosure = function() return hideCard(secondCard) end
		timer.performWithDelay(0200, myclosure, 1)
		
		local myclosure = function() return resumeEngine() end
		timer.performWithDelay(0500, myclosure, 1)
		
		shell.moveBoy("sad")
		playAudio("wrongCard")
		
	end
	
end

resumeEngine = function()

	firstCard = nil
	secondCard = nil

	gameStatus = "playing"
	
end

-- Scaled
playReward = function()
	
	--audio
	fadeOutMusic()
	local myclosure = function() return playAudio("endscreen") end
	timer.performWithDelay(1000, myclosure, 1)
	
	
	local myclosure = function() return shell.hideBoy() end
	timer.performWithDelay(300, myclosure, 1)
	
	--transition.to(engineG.black, {delay=500, alpha=1, transition=easing.outQuad})


	
	_G.isPaused = true
	
	--------
	
	transition.to(engineG.rewardG,   {delay=700, time=1000, y=0, transition=easing.outQuad})
	
	engineG.rewardG.learn1.alpha = 0
	engineG.rewardG.learn2.alpha = 0
	engineG.rewardG.challenge1.alpha = 0
	engineG.rewardG.challenge2.alpha = 0
	
	if(_G.gameMode == "learn")then
	
		-- Save level progress
		local nextLevel = curLevel + 1;
		if(nextLevel > 3 and not isPremium) then
			--print("Leaving current level as this because premium version is required to go above")
		else
			saveLevelProgress(nextLevel)	
		end
				
		engineG.playAgain.x = 100
		engineG.back.x = 414
		
		if(curLevel < #data.allData)then
			
			engineG.rewardG.learn1.alpha = 1
			
			transition.to(engineG.playAgain, {delay=2000, time=500, y=215, alpha=1, transition=easing.outQuad})
			transition.to(engineG.next,      {delay=2050, time=500, y=215, alpha=1, transition=easing.outQuad})
			transition.to(engineG.back,      {delay=2100, time=500, y=215, alpha=1, transition=easing.outQuad})
			
			-- Boolean for easy disabling of the button
			if inviteFriendsAllowed then
				transition.to(engineG.invite,    {delay=2150, time=500, y=265, alpha=1, transition=easing.outQuad})
			end
		else
			
			engineG.rewardG.learn2.alpha = 2
			
			transition.to(engineG.playAgain, {delay=2000, time=500, y=215, alpha=1, transition=easing.outQuad})
			transition.to(engineG.challenge, {delay=2050, time=500, y=215, alpha=1, transition=easing.outQuad})
			transition.to(engineG.back,      {delay=2100, time=500, y=215, alpha=1, transition=easing.outQuad})
			-- Only if inviteFriends button is activated
			if inviteFriendsAllowed then
				transition.to(engineG.invite,    {delay=2150, time=500, y=265, alpha=1, transition=easing.outQuad})
			end
		end
		
	elseif(_G.gameMode == "challenge")then
		
		timer.cancel(challengeTimer)
		shell.pourSand("stop")
		
		engineG.playAgain.x = 158.5
		engineG.back.x = 353.5
		
		transition.to(engineG.playAgain, {delay=2000, time=500, y=215, alpha=1, transition=easing.outQuad})
		transition.to(engineG.back,      {delay=2100, time=500, y=215, alpha=1, transition=easing.outQuad})
		-- Only if inviteFriends button is activated
		if inviteFriendsAllowed then
			transition.to(engineG.invite,    {delay=2150, time=500, y=265, alpha=1, transition=easing.outQuad})
		end

		--check times
		if( (curTime < _G.recordTime) or (_G.recordTime == 0) )then
			
			local timeDiff = math.abs(_G.recordTime - curTime)
			engineG.rewardG.timeText.text = "" .. timeDiff
			engineG.rewardG.challenge2.alpha = 1
		
			shell.updateTimer("best", formatTime("BEST TIME: ", curTime) )
			_G.recordTime  = curTime
			_G.allSaveData[1] = _G.recordTime
			storage.writeNewData(_G.allSaveData)
			
		else
			
			engineG.rewardG.challenge1.alpha = 1
			
		end
	
	end
		
	local myclosure = function() return setToReward() end
	timer.performWithDelay(2700, myclosure, 1)
	
end

setToReward = function ()

	gameStatus = "reward"
	
end

setChallengeMode = function()
	-- Set the toggle button to challenge, you have to pass
	-- current mode, and it will toggle to the other one.
	shell.toggleOverlayMenu("learn")
	-- Set the mode to challenge
	_G.gameMode = "challenge"
	-- Save the game state
	saveGameMode(_G.gameMode)
end

onRewardTouch = function(e)

	if(_G.isPaused)then
		--return
	end

	if(gameStatus ~= "reward")then
		return
	end
	
	if(e.phase == "began")then
		
		playAudio("click")
		
		
		-- If you click invite friends
		-- native popup appears for either email or sms
		if(e.target.id == "invite") then
			local friendsLibrary = require("invitefriends")
			friendsLibrary.inviteFriends();
			return true;
		end

			
		if(e.target.id == "play again")then
			
			
			local myclosure = function() return resetGame() end
			timer.performWithDelay(1250, myclosure, 1)	
			
		elseif(e.target.id == "back")then
			-- Save progress.
			if(_G.gameMode == "learn" and curLevel == 7) then
				setChallengeMode()
			end
			
			local myclosure = function() return returnToMenu() end
			timer.performWithDelay(1250, myclosure, 1)
			
		elseif(e.target.id == "next")then
			
			curLevel = curLevel + 1
			if(curLevel > 3) then
				if(not isPremium) then
					-- Temporary handler
					local function onComplete( event )
						if "clicked" == event.action then
							local i = event.index
							if 1 == i then
									unlockPremiumClosure()
									print("Buying..")
	
							elseif 2 == i then
									cancelled = true;
							end
						end
					end
					
					native.showAlert("Fremium Version"," You finished all the levels in the free version. Would you like to update to premium?", {"Buy it", "Not now"}, onComplete)
					return true;
				end
				
			end
			
			
			local myclosure = function() return resetGame() end
			timer.performWithDelay(1250, myclosure, 1)
			
		elseif(e.target.id == "challenge")then
			setChallengeMode()
			
			local myclosure = function() return resetGame() end
			timer.performWithDelay(1250, myclosure, 1)
			
		end
		
		-- Change the status of the game
		gameStatus = "resetting"
		-- Hide Rewards
		hideReward()
		-- Fade out the end audio
		-- The method is in the main
		fadeEndOut()	
	end
	
end

-- Scaled
hideReward = function()

	transition.to(engineG.rewardG,   {delay=0000, time=1000, y=-250, transition=easing.outQuad})
	--Transition out the reward screen
	transition.to(engineG.playAgain, {delay=250, time=500, y=220, alpha=0, transition=easing.outQuad})
	transition.to(engineG.next,      {delay=350, time=500, y=220, alpha=0, transition=easing.outQuad})
	transition.to(engineG.challenge, {delay=350, time=500, y=220, alpha=0, transition=easing.outQuad})
	transition.to(engineG.back,      {delay=400, time=500, y=220, alpha=0, transition=easing.outQuad})
	-- Fade out invite friends button
	if inviteFriendsAllowed then
		transition.to(engineG.invite, {delay=400, time=500, y=270, alpha=0, transition=easing.outQuad})
	end
	
	--transition.to(engineG.black,     {delay=400, time=500, y=220, alpha=0, transition=easing.outQuad})
	
end

resetGame = function()	
	killEngine()
	
	buildEngine(curLevel)
	
end

-- is called from shell
function resetCurrentGame(type)
	
	if(challengeTimer)then
		timer.cancel(challengeTimer)
	end

	local delayer = 200
	for i=1, cardGroup.numChildren do
		local tempCard = cardGroup[i]
		
		local function deleteCard()
			display.remove(tempCard)
			tempCard = nil;
		end
				
		transition.to(tempCard, {delay=delayer, time=250, y=tempCard.y - 10, alpha=0, transition=easing.outQuad, onComplete = deleteCard})
		delayer = delayer + 100
	end

	
	local myclosure = function() return shell.hideBoy() end
	timer.performWithDelay(1000, myclosure, 1)
	
	if(type == "replay")then
	
		local myclosure = function() return resetGame() end
		timer.performWithDelay(1500, myclosure, 1)
		
	end
	
	if(type == "menu")then
		
		local myclosure = function() return returnToMenu() end
		timer.performWithDelay(1500, myclosure, 1)
		
	end
	
end

returnToMenu = function()
	shell.pauseGame()
	killEngine()
	shell.backToMenu()	
end

-------------------------- UTILS

tableShuffle = function(t)
        
		math.randomseed(os.time())
        assert(t, "tableShuffle() expected a table, got nil")
        local iterations = #t
        local j
        for i = iterations, 2, -1 do
                j = math.random(i)
                t[i], t[j] = t[j], t[i]
        end

end

groupShuffle = function(t)
        
		math.randomseed(os.time())
        assert(t, "groupShuffle() expected a group, got nil")
        local iterations = t.numChildren
        local j
        for i = iterations, 2, -1 do
                j = math.random(iterations)
                
                local oneToFour = math.random(4)
                if(oneToFour == 1) then
                	t[i]:toBack()
                	t[j]:toFront()
                elseif(oneToFour == 2) then
                	t[i]:toFront()
                	t[j]:toBack()
                elseif(oneToFour == 3) then
                    t[i]:toBack()
                	t[j]:toBack()
                else
                	t[i]:toFront()
                	t[j]:toFront()
                end

        end

end

-------------------------- CLEAN UP
function killEngine()
	display.remove(engineG)
	engineG = {}
end


